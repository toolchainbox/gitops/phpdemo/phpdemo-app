<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


function get_system_load($coreCount = 2, $interval = 1) {
  $rs = sys_getloadavg();
  $interval = $interval >= 1 && 3 <= $interval ? $interval : 1;
  $load = $rs[$interval];
  return round(($load * 100) / $coreCount,2);
}

function get_connections() {
  if (function_exists('exec')) {
    $www_total_count = 0;
    @exec ('netstat -an | egrep \':80|:443\' | awk \'{print $5}\' | grep -v \':::\*\' |  grep -v \'0.0.0.0\'', $results);

    foreach ($results as $result) {
      $array = explode(':', $result);
      $www_total_count ++;
      if (preg_match('/^::/', $result)) {
        $ipaddr = $array[3];
      } else {
        $ipaddr = $array[0];
      }
      if (!in_array($ipaddr, $unique)) {
        $unique[] = $ipaddr;
        $www_unique_count ++;
      }
    }
    unset ($results);

    return count($unique);
  }
}

function get_memory_usage() {
  $free = shell_exec('free');
  $free = (string)trim($free);
  $free_arr = explode("\n", $free);
  $mem = explode(" ", $free_arr[1]);
  $mem = array_filter($mem);
  $mem = array_merge($mem);
  $memory_usage = $mem[2] / $mem[1] * 100;

  return round($memory_usage,2);
}

function get_ip() {
  return $_SERVER['SERVER_ADDR'];
}


function createName() {

    
  //PHP array containing forenames.
  $names = array(
    'Christopher',
    'Ryan',
    'Ethan',
    'John',
    'Zoey',
    'Sarah',
    'Michelle',
    'Samantha',
  );

  //PHP array containing surnames.
  $surnames = array(
    'Walker',
    'Thompson',
    'Anderson',
    'Johnson',
    'Tremblay',
    'Peltier',
    'Cunningham',
    'Simpson',
    'Mercado',
    'Sellers'
  );

  //Generate a random forename.
  $random_name = $names[mt_rand(0, sizeof($names) - 1)];
  //Generate a random surname.
  $random_surname = $surnames[mt_rand(0, sizeof($surnames) - 1)];

  return $random_name . ' ' . $random_surname;
}

function getConnectionStatus() {

      if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') { 
        $ret = '
        <span style="color: #339966;">
          <strong>
          HTTPS / Abgesichert
          </strong>
        </span>
        ';
      } else {
        $ret = '
        <span style="color: #993300;">
          <strong>
          HTTP / Unsicher
          </strong>
        </span>
        ';
      }
  
  return $ret;

}

function printEnvVar($varname) {

  switch ($varname) {

    // Abfrage, ob Variable 18 ist
      case "SVA_USER":
        // Anweisung falls zutrifft
        $ausgabe = "JACKPOT ENV Var:<br> " . $varname . " / Value: " .  getenv("$varname");
        break;
    
    // Standard, falls zuvor nichts zutrifft
      default:
        $ausgabe = "ENV:<br> " . $varname . " / Value: " .  getenv("$varname");
    }
    
  
  return $ausgabe;
}


// class DBConnection {
  
//   // Properties
//   public $servername = "database";
//   public $database = "sva";
//   public $username = "sva";
//   public $password = "svarocks";
//   public $port = 3306;

//   // Methods
//   function connectDB() {
//     $this->mysqli = new mysqli($this->servername, $this->username, $this->password, $this->database);
//   }

//   function connectDBStatus() {
//     $this->connectDB();
//     $conn=$this->mysqli;
//     if ($conn->connect_error) {
//       $msg = "Connection failed: " . $conn->connect_error;
//     } else {
//       $msg = "Connection successful: ";
//     }
  
//     return $msg;
//   }
// } 



// $mydb = new DBConnection;
// $mydb->connectDB();


$api_object = new stdClass;
$api_object->name = strval( createName() );
$api_object->load = strval( get_system_load() );
$api_object->memory = strval( get_memory_usage() );
#$api_object->connections = strval( get_connections() );
$api_object->ip = strval( get_ip() );
$api_object->tlsstatus = strval( getConnectionStatus() );
#$api_object->mysqlstatus = strval( "s" );
$api_object->envsva = printEnvVar("sva");
$api_object->envsvausername = printEnvVar("SVA_USER");


echo json_encode($api_object);
?>