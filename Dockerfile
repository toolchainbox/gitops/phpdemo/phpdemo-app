FROM registry.access.redhat.com/ubi8/ubi:8.0

RUN yum install -y --disableplugin=subscription-manager \
                   --nodocs openssl httpd php-fpm php php-common php-json net-tools procps-ng mod_ssl php-pdo \
    && yum --exclude=filesystem upgrade -y \
    && yum clean all \
    && mkdir /run/php-fpm \
    && chgrp -R 0 /var/www/html /var/log/httpd /var/run/httpd/ /run/php-fpm \
    && chmod -R g+rwx /var/www/html /var/log/httpd /var/run/httpd/ /run/php-fpm \
    && mkdir -p /usr/local/etc/ssl/certs/ \
    && openssl req -newkey rsa:4096 -nodes -sha256 \
            -keyout /usr/local/etc/ssl/certs/tls.key \
            -x509 -days 365 -subj "/CN=host.compute.local" \
            -out /usr/local/etc/ssl/certs/tls.crt \
    && chmod -R 777 /usr/local/etc/ssl/certs/* /var/log/php-fpm/

EXPOSE 8080 8443

COPY httpd.conf /etc/httpd/conf/httpd.conf
COPY api.php /var/www/html/api.php 
COPY index.html /var/www/html/index.html 
COPY ssl.conf /etc/httpd/conf.d/ssl.conf
COPY www.conf /etc/php-fpm.d/www.conf
COPY php.conf /etc/httpd/conf.d/php.conf
#USER 1001
CMD ["/bin/sh","-c","php-fpm && httpd -D FOREGROUND"]
